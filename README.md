# Payment Swish

## INTRODUCTION
Provides an integration with [Swish](https://www.getswish.se/) payment gateway, 
when using Payment.

## DEPENDENCIES
[Payment](https://www.drupal.org/project/payment)

## REQUIREMENTS
Default Drupal 8 requirenments.

## INSTALLATION
Install as you would normally install a contributed Drupal module.
See https://www.drupal.org/node/895232 for further information.

## CONFIGURATION
1) Enable Payment Swish module in: `admin/modules`.
2) Enable Payment Method in: `admin/config/services/payment/method`.
3) Configure properly according your API settings in: 
`admin/config/services/payment/method/configuration/payment_swish`.
