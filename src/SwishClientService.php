<?php

namespace Drupal\payment_swish;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Handler\CurlHandler;
use GuzzleHttp\HandlerStack;

/**
 * Class SwishClientService.
 */
class SwishClientService {

  use StringTranslationTrait;

  const SWISH_PRODUCTION_URL = 'https://cpc.getswish.net/swish-cpcapi/api/v1';

  const SWISH_TEST_URL = 'https://mss.cpc.getswish.net/swish-cpcapi/api/v1';

  /**
   * Base url to connect to getswish API.
   *
   * @var string
   */
  private $baseURL;

  /**
   * The HTTP client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * The HTTP client configs.
   *
   * @var array
   */
  protected $httpClientConfigs;

  /**
   * The Swish number of the payee.
   *
   * It needs to match with Merchant Swish number.
   *
   * @var string
   */
  protected $payeeAlias;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $account;

  /**
   * The logger service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $logger;

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * Constructs a new SwishClientService object.
   *
   * @param \GuzzleHttp\ClientInterface $http_client
   *   The HTTP Client.
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The currently logged in account.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger
   *   The logger factory.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   */
  public function __construct(ClientInterface $http_client, Connection $connection, AccountInterface $account, LoggerChannelFactoryInterface $logger, TimeInterface $time) {
    $this->httpClient = $http_client;
    $this->connection = $connection;
    $this->account = $account;
    $this->logger = $logger->get('payment_swish.transaction');
    $this->time = $time;
  }

  /**
   * Set configs.
   *
   * @param array $configuration
   *   Base configs of service. Required before using.
   */
  public function setConfigs(array $configuration) {
    $key = $configuration['private_key'];
    $key_pw = $configuration['private_key_pw'];
    if ($key_pw) {
      $key = [$key, $key_pw];
    }

    $cert = $configuration['client_cert'];
    $cert_pw = $configuration['client_cert_pw'];
    if ($cert_pw) {
      $cert = [$cert, $cert_pw];
    }

    $this->baseURL = ($configuration['live_mode'] ? self::SWISH_PRODUCTION_URL : self::SWISH_TEST_URL);

    $ca_verification = !($configuration['disable_ca_verification']);
    $root_ca = $configuration['ca_cert'];

    $this->httpClientConfigs = [
      'http_errors' => FALSE,
      'verify' => ($root_ca ? $root_ca : $ca_verification),
      'cert' => $cert,
      'ssl_key' => $key,
      'handler' => HandlerStack::create(new CurlHandler()),
    ];

    $this->payeeAlias = $configuration['payee_alias'];
  }

  /**
   * Main method to make payment.
   *
   * @param int $payerAlias
   *   Merchant number.
   * @param float $amount
   *   Ammount of payment.
   * @param int $payeePaymentReference
   *   Payment id.
   * @param string $message
   *   Message for user.
   *
   * @return bool|mixed
   *   Transaction id if succeeded
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function createPaymentRequest($payerAlias, $amount, $payeePaymentReference = NULL, $message = NULL) {
    try {
      $callbackUrl = Url::fromRoute("payment_swish.callback_update", [], [
        'https' => TRUE,
        'absolute' => TRUE,
      ])->toString();

      $payerAlias = self::normalizePhoneNo($payerAlias);

      $data = [
        "payeePaymentReference" => $payeePaymentReference,
        "callbackUrl" => $callbackUrl,
        "payerAlias" => $payerAlias,
        "payeeAlias" => $this->payeeAlias,
        "amount" => $amount,
        // Currently supported only value SEK.
        "currency" => 'SEK',
        "message" => $message,
      ];

      /** @var \GuzzleHttp\Psr7\Response $response */
      $response = $this->sendRequest('POST', '/paymentrequests', $data);
      $statusCode = $response->getStatusCode();

      $transactionId = FALSE;
      if ($statusCode == 201) {
        $location = $response->getHeader('Location');
        $location_parts = explode("/", reset($location));
        $transactionId = array_pop($location_parts);
      }
      else {
        $this->logger->error($this->t('Error @code', ['@code' => $statusCode]));
        drupal_set_message($this->t('Error @code', ['@code' => $statusCode]), 'error');
      }

      try {
        $this->connection->insert('payment_swish_transaction')->fields([
          'transaction_id' => $transactionId,
          'payee_payment_reference' => $payeePaymentReference,
          'uid' => $this->account->id(),
          'payer_alias' => $payerAlias,
          'message' => $message,
          'amount' => $amount,
          'status' => 'CREATED',
          'created' => $this->time->getCurrentTime(),
        ])->execute();
      }
      catch (\Exception $e) {
        $this->logger->error($e->getMessage());
        drupal_set_message($this->t('An error occurred @e', ['@e' => $e->getMessage()]), 'error');

        return FALSE;
      }

      // Debug
      // $this->retrievePaymentRequest($transactionId);
      return $transactionId;
    }
    catch (\Exception $e) {
      $this->logger->error($e->getMessage());
      drupal_set_message($this->t('An error occurred @e', ['@e' => $e->getMessage()]), 'error');
    }

    return FALSE;
  }

  /**
   * Check payment status.
   *
   * @param string $transactionId
   *   Swish transaction id.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function retrievePaymentRequest($transactionId) {
    try {
      $response = $this->sendRequest('GET', '/paymentrequests/' . $transactionId);
      $statusCode = $response->getStatusCode();
      if ($statusCode == 200) {
        $data = json_decode($response->getBody());
        $data = (array) $data;
        $items = [
          'paymentReference',
          'status',
          'datePaid',
          'errorCode',
          'errorMessage',
          'additionalInformation',
        ];
        $fields = [];
        foreach ($items as $item) {
          if (!empty($data[$item])) {
            if ($item == 'datePaid') {
              $fields[$item] = strtotime($data[$item]);
            }
            else {
              $fields[$item] = $data[$item];
            }
          }
        }
        $fields += ['changed' => $this->time->getCurrentTime()];

        try {
          $this->connection->merge('payment_swish_transaction')->keys([
            'transaction_id' => $transactionId,
          ])->fields($fields)->execute();
        }
        catch (\Exception $e) {
          $this->logger->error($e->getMessage());
        }
      }
      else {
        $this->logger->error($this->t('Error @code', ['@code' => $statusCode]));
        drupal_set_message($this->t('Error @code', ['@code' => $statusCode]), 'error');
      }
    }
    catch (\Exception $e) {
      $this->logger->error($e->getMessage());
      drupal_set_message($this->t('Some other error @e', ['@e' => $e->getMessage()]), 'error');
    }
  }

  /**
   * Helper to use guzzle client.
   *
   * @param string $method
   *   POST || GET.
   * @param string $endpoint
   *   Base url.
   * @param array $data
   *   Additional data to send via json.
   *
   * @return bool|\Psr\Http\Message\ResponseInterface
   *   Result of responce.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  protected function sendRequest($method, $endpoint, array $data = NULL) {
    if ($data) {
      $return = $this->httpClient->request($method, $this->baseURL . $endpoint, $this->httpClientConfigs + ['json' => $data]);
    }
    else {
      $return = $this->httpClient->request($method, $this->baseURL . $endpoint, $this->httpClientConfigs);
    }

    return $return;
  }

  /**
   * Converted phone number to swish standard.
   *
   * @param string $phoneNo
   *   Phone number from user input.
   *
   * @return string
   *   Stabilized phone number.
   */
  protected static function normalizePhoneNo($phoneNo) {
    $phoneNo = preg_replace("/[^0-9]*/", "", $phoneNo);
    if (strpos($phoneNo, "0") === 0) {
      $phoneNo = substr($phoneNo, 1);
    }
    if (strpos($phoneNo, "46") === FALSE) {
      $phoneNo = "46" . $phoneNo;
    }

    return $phoneNo;
  }

  /**
   * Helper to validate phone number.
   *
   * @param string $phoneNo
   *   Phone number from user input.
   *
   * @return bool
   *   Validated.
   */
  public static function validatePhoneNo($phoneNo) {
    $phoneNo = self::normalizePhoneNo($phoneNo);

    return strlen($phoneNo) >= 8 && strlen($phoneNo) <= 15;
  }

}
