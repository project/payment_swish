<?php

namespace Drupal\payment_swish\Plugin\Payment\Method;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Database\Connection;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\Core\Utility\Token;
use Drupal\payment\EventDispatcherInterface;
use Drupal\payment\OperationResult;
use Drupal\payment\Plugin\Payment\Method\Basic;
use Drupal\payment\Plugin\Payment\Status\PaymentStatusManagerInterface;
use Drupal\payment_swish\SwishClientService;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\payment\Response\Response;

/**
 * A Swish payment method.
 *
 * @PaymentMethod(
 *   label = "Swish",
 *   deriver = "\Drupal\payment_swish\Plugin\Payment\Method\SwishDeriver",
 *   id = "payment_swish",
 *   operations_provider =
 *   "\Drupal\payment_swish\Plugin\Payment\Method\SwishOperationsProvider",
 * )
 */
class Swish extends Basic {

  /**
   * The Swish client.
   *
   * @var \Drupal\payment_swish\SwishClientService
   */
  protected $swishClient;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * {@inheritdoc}
   */
  public function __construct($configuration, string $plugin_id, $plugin_definition, ModuleHandlerInterface $module_handler, EventDispatcherInterface $event_dispatcher, Token $token, PaymentStatusManagerInterface $payment_status_manager, SwishClientService $swish_client, Connection $connection) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $module_handler, $event_dispatcher, $token, $payment_status_manager);
    $this->swishClient = $swish_client;
    $this->connection = $connection;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static($configuration, $plugin_id, $plugin_definition, $container->get('module_handler'), $container->get('payment.event_dispatcher'), $container->get('token'), $container->get('plugin.manager.payment.status'), $container->get('payment_swish.client'), $container->get('database'));
  }

  /**
   * {@inheritdoc}
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  protected function doExecutePayment() {
    $configuration = $this->getPluginDefinition();
    $this->swishClient->setConfigs($configuration);

    $totalAmount = 0;
    foreach ($this->getPayment()->getLineItems() as $line_item) {
      $totalAmount += $line_item->getTotalAmount();
    }

    $this->swishClient->createPaymentRequest($this->configuration['cellphone_number'], $totalAmount, $this->getPayment()
      ->id(), $this->configuration['message']);
  }

  /**
   * {@inheritdoc}
   */
  public function getPaymentExecutionResult() {
    parent::getPaymentExecutionResult();

    $trans_id = $this->connection->select('payment_swish_transaction', 'pst', [])
      ->fields('pst', ['transaction_id'])
      ->condition('payee_payment_reference', $this->getPayment()->id())
      ->execute()
      ->fetchField();

    if ($trans_id) {
      $response = new Response(Url::fromRoute("payment_swish.callback_pending", ['trans_id' => $trans_id], [
        'absolute' => TRUE,
      ]));
    }
    else {
      $response = new Response(Url::fromRoute("payment_swish.callback_error", [], [
        'absolute' => TRUE,
      ]));
    }

    return new OperationResult($response);
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {

    $elements['cellphone_number'] = [
      '#type' => 'tel',
      '#title' => $this->t('Cellphone number'),
      '#description' => $this->t('Enter cellphone number to the device where Swish is installed'),
      '#required' => TRUE,
    ];

    $elements['message'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Message'),
      '#description' => $this->t('Enter a message here.'),
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {

    $parents = $form['cellphone_number']['#parents'];
    array_pop($parents);
    $values = $form_state->getValues();
    $values = NestedArray::getValue($values, $parents);

    if (!$this->swishClient::validatePhoneNo($values['cellphone_number'])) {
      $form_state->setErrorByName('cellphone_number', t('Enter a valid phone no.'));
    }

    parent::validateConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    $parents = $form['cellphone_number']['#parents'];
    array_pop($parents);
    $values = $form_state->getValues();
    $values = NestedArray::getValue($values, $parents);

    $this->configuration['cellphone_number'] = $values['cellphone_number'];
    $this->configuration['message'] = $values['message'];
  }

}
