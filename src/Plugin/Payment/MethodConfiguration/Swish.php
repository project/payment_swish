<?php

namespace Drupal\payment_swish\Plugin\Payment\MethodConfiguration;

use Drupal\Core\Form\FormStateInterface;
use Drupal\payment\Plugin\Payment\MethodConfiguration\Basic;
use Drupal\Component\Utility\NestedArray;

/**
 * Provides the configuration for the payment_swish payment method plugin.
 *
 * @PaymentMethodConfiguration(
 *   description = @Translation("A Swish payment method."),
 *   id = "payment_swish",
 *   label = @Translation("Swish")
 * )
 */
class Swish extends Basic {

  /**
   * Implements a form API #process callback.
   */
  public function processBuildConfigurationForm(array &$element, FormStateInterface $form_state, array &$form) {

    $element['swish'] = [
      '#type' => 'container',
      '#weight' => -99,
    ];

    $element['swish']['payee_alias'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Payee alias'),
      '#description' => $this->t('Enter your Swish number here.'),
      '#default_value' => $this->configuration['payee_alias'] ?? '',
      '#required' => TRUE,
    ];

    $element['swish']['private_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Private key'),
      '#description' => $this->t('Path and filename to the private key file. Should reside outside if www-root if possible.'),
      '#default_value' => $this->configuration['private_key'] ?? '',
      '#required' => TRUE,
    ];

    $element['swish']['private_key_pw'] = [
      '#type' => 'password',
      '#title' => $this->t('Private key password'),
      '#description' => $this->t('Password for the private key, if any.'),
      '#default_value' => $this->configuration['private_key_pw'] ?? '',
    ];

    $element['swish']['client_cert'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Client cert'),
      '#description' => $this->t('Path and filename to the client cert file. Should reside outside if www-root if possible.'),
      '#default_value' => $this->configuration['client_cert'] ?? '',
      '#required' => TRUE,
    ];

    $element['swish']['client_cert_pw'] = [
      '#type' => 'password',
      '#title' => $this->t('Client cert password'),
      '#description' => $this->t('Password for the client certificate, if any.'),
      '#default_value' => $this->configuration['client_cert_pw'] ?? '',
    ];

    $element['swish']['ca_cert'] = [
      '#type' => 'textfield',
      '#title' => $this->t('CA root cert'),
      '#description' => $this->t('Path and filename to the CA root cert file. Omit this and you must either import the CA on the server or disable CA verification belove.'),
      '#default_value' => $this->configuration['ca_cert'] ?? '',
    ];

    $element['swish']['disable_ca_verification'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Disable CA verification'),
      '#description' => $this->t('Check this to disable CA verification.'),
      '#default_value' => $this->configuration['disable_ca_verification'] ?? '',
    ];

    $element['swish']['live_mode'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Live mode'),
      '#description' => $this->t('Check this to call live server.'),
      '#default_value' => $this->configuration['live_mode'] ?? '',
    ];

    parent::processBuildConfigurationForm($element, $form_state, $form);

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    $parents = $form['plugin_form']['swish']['#parents'];
    array_pop($parents);
    $values = $form_state->getValues();
    $values = NestedArray::getValue($values, $parents);

    $this->configuration['payee_alias'] = $values['swish']['payee_alias'];
    $this->configuration['private_key'] = $values['swish']['private_key'];
    $this->configuration['private_key_pw'] = $values['swish']['private_key_pw'];
    $this->configuration['client_cert'] = $values['swish']['client_cert'];
    $this->configuration['client_cert_pw'] = $values['swish']['client_cert_pw'];
    $this->configuration['ca_cert'] = $values['swish']['ca_cert'];
    $this->configuration['disable_ca_verification'] = $values['swish']['disable_ca_verification'];
    $this->configuration['live_mode'] = $values['swish']['live_mode'];
  }

  /**
   * Added additional settings.
   *
   * @return array
   *   Configs.
   */
  public function getDerivativeConfiguration() {
    return [
      'payee_alias' => $this->configuration['payee_alias'] ?? '',
      'private_key' => $this->configuration['private_key'] ?? '',
      'private_key_pw' => $this->configuration['private_key_pw'] ?? '',
      'client_cert' => $this->configuration['client_cert'] ?? '',
      'client_cert_pw' => $this->configuration['client_cert_pw'] ?? '',
      'ca_cert' => $this->configuration['ca_cert'] ?? '',
      'disable_ca_verification' => $this->configuration['disable_ca_verification'] ?? '',
      'live_mode' => $this->configuration['live_mode'] ?? '',
    ];
  }

}
