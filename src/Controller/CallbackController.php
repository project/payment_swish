<?php

namespace Drupal\payment_swish\Controller;

use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Url;
use Drupal\Core\Controller\ControllerBase;
use Drupal\payment\Plugin\Payment\Status\PaymentStatusManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class CallbackController.
 *
 * @package Drupal\payment_swish\Controller
 */
class CallbackController extends ControllerBase {

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * The payment status manager.
   *
   * @var \Drupal\payment\Plugin\Payment\Status\PaymentStatusManagerInterface
   */
  protected $paymentStatusManager;

  /**
   * The payment storage.
   *
   * @var \Drupal\payment\entity\Payment\PaymentStorage
   */
  protected $paymentStorage;

  /**
   * The logger service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $logger;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database'),
      $container->get('plugin.manager.payment.status'),
      $container->get('entity_type.manager')->getStorage('payment'),
      $container->get('logger.factory'));
  }

  /**
   * {@inheritdoc}
   */
  public function __construct(Connection $connection, PaymentStatusManagerInterface $payment_status_manager, EntityStorageInterface $payment_storage, LoggerChannelFactoryInterface $logger) {
    $this->connection = $connection;
    $this->paymentStatusManager = $payment_status_manager;
    $this->paymentStorage = $payment_storage;
    $this->logger = $logger->get('payment_swish.transaction');
  }

  /**
   * Complete.
   *
   * This is called by Swish service provider top update
   * payment status on our side.
   *
   * @return string
   *   Return Hello string.
   */
  public function update() {
    $request = json_decode(file_get_contents("php://input"));
    if ($request->id) {
      $items = [
        'error_code' => 'errorCode',
        'error_message' => 'errorMessage',
        'payment_reference' => 'paymentReference',
        'status' => 'status',
        'paid' => 'datePaid',
      ];
      $fields = [];
      foreach ($items as $key => $item) {
        if (!empty($request->{$item})) {
          if ($item == 'datePaid') {
            $fields[$key] = strtotime($request->{$item});
          }
          else {
            $fields[$key] = $request->{$item};
          }
        }
      }

      try {
        $this->connection->merge('payment_swish_transaction')->keys([
          'transaction_id' => $request->id,
        ])->fields($fields)->execute();
      }
      catch (\Exception $e) {
        $this->logger->error($e->getMessage());
      }

    }

    return new JsonResponse(TRUE);
  }

  /**
   * Pending payment.
   *
   * This is the page that display the pengind payment information.
   *
   * @return array
   *   Return Hello string.
   */
  public function pending($trans_id) {
    if (!empty($_GET['destination'])) {
      $destination = $_GET['destination'];
    }
    if (empty($destination)) {
      $destination = Url::fromRoute("payment_swish.callback_complete", ['trans_id' => $trans_id])
        ->toString();
    }
    $loading = drupal_get_path('module', 'payment_swish') . '/assets/img/loading.gif';

    return [
      '#title' => $this->t('Waiting for payment'),
      '#attached' => [
        'library' => ['payment_swish/payment-swish-pending'],
        'drupalSettings' => [
          'transactionId' => $trans_id,
          'destination' => $destination,
        ],
      ],
      '#markup' => "<div class='payment-swish-wrapper'><p>" . $this->t('Please open the Swish App and complete the payment.') . "</p><p><img src=\"/$loading\" class=\"payment-swish-loading\" /></p></div>",
    ];
  }

  /**
   * Poll status.
   *
   * This is called from our pending page to see if the status has changed.
   *
   * @param string $trans_id
   *   Transaction id.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   Response result with status.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function poll($trans_id) {
    $status = $this->connection->select('payment_swish_transaction', 'pst', [])
      ->fields('pst', ['status'])
      ->condition('transaction_id', $trans_id)
      ->execute()
      ->fetchField();

    if ($status == 'PAID') {
      $payment_id = $this->connection->select('payment_swish_transaction', 'pst', [])
        ->fields('pst', ['payee_payment_reference'])
        ->condition('transaction_id', $trans_id)
        ->execute()
        ->fetchField();

      $payment = $this->paymentStorage->load($payment_id);
      $payment->setPaymentStatus($this->paymentStatusManager->createInstance('payment_success'));
      $payment->save();
    }

    $return = ['status' => $status];

    return new JsonResponse($return);
  }

  /**
   * Error page.
   *
   * @return array
   *   Return Error message string.
   */
  public function error() {
    return [
      '#title' => $this->t('An error occurred'),
      '#type' => 'markup',
      '#markup' => '<p>' . $this->t('Check you phone if you have any pending payment blocking this payment.') . '</p>',
    ];
  }

  /**
   * Done page.
   *
   * @return array
   *   Return Complete page
   */
  public function complete($trans_id) {
    return [
      '#title' => $this->t('Payment complete'),
      '#type' => 'markup',
      '#markup' => "<p>" . $this->t('Your Swish payment was completed successfully. Thanks!') . "</p>",
    ];
  }

}
